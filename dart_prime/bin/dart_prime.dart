import 'package:dart_prime/dart_prime.dart' as dart_prime;
import 'dart:io';

void main(List<String> arguments) {
  print("input number :");
  int x = int.parse(stdin.readLineSync()!);
  isPrime(x);
}

void isPrime(int x) {
  bool Prime = true;
  for (int i = 2; i <= x / 2; i++) {
    if (x % i == 0) {
      Prime = false;
      break;
    }
    if (Prime || x == 1) {
      print("$x is a Prime Number");
    } else {
      print("$x is Not a Prime Number");
    }
  }
}
